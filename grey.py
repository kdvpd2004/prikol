def is_gray(digit):
    return digit == 'F0F0F0'


def to_gray(digit):
    x = ['F0', 'F0', 'F0']
    y = []
    for i in range(len(digit)):
        if i % 2 != 0:
            y.append(f'{digit[i - 1]}{digit[i]}')
    s1 = hex(int(x[0], 16) - int(y[0], 16)).split('x')[-1]
    s2 = hex(int(x[1], 16) - int(y[1], 16)).split('x')[-1]
    s3 = hex(int(x[2], 16) - int(y[2], 16)).split('x')[-1]
    s = (s1, s2, s3)
    return f'#{s}'


def main():
    print(f'[exit]Выход из программы')
    while True:
        digit = input('Введите число:#')
        if digit == 'exit':
            break
        if len(digit) != 6:
            print('Ошибка, введите число, состоящее из 6 цифр', end='\n\n')
            continue
        try:
            hex(int(digit, 16))
        except ValueError:
            print('Ошибка, введите 16-ичное число', end='\n\n')
            continue
        if int(digit, 16) > int('F0F0F0', 16):
            print('Ошибка, введите число меньшее "#F0F0F0"', end='\n\n')
            continue
        print(f'Введёное вами число является серым:{is_gray(digit)}\n'
              f'Чтобы получить серый необходимо прибавить к красному, синему, зелёному: {to_gray(digit)}', end='\n\n')


if __name__ == '__main__':
    main()
print('Changes in project')
